/* 
 * The MIT License
 *
 * Copyright 2014 insusong.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// TODO --
//   db is a global variable. Pass as a variable
//   group the functions and separate into multiple files

// WebSQL Ref: http://cordova.apache.org/docs/en/2.4.0/cordova_storage_storage.md.html
var db = openDatabase('mydb', '1.0', 'DB for question and answer', 2 * 1024 * 1024);

//GET DATA FOR FOLDER LIST
function showlistview() {
    var sub_id3;
    //open DB temp that store sub_ID and try to get the value
    db.transaction(function(tx) {
        var statement = 'SELECT sub_IDtemp FROM temp;';
        tx.executeSql(statement, [], function(tx, results) {
            var len3 = results.rows.length;
            sub_id3 = results.rows.item(len3 - 1).sub_IDtemp;
        });
    });

    $("#weeklist").empty();

    db.transaction(function(tx) {
        var statement = 'SELECT qset_id, q_set_name FROM questionSet WHERE sub_ID = ' + sub_id3 + ';';
        tx.executeSql(statement, [], function(tx, results) {
            var len = results.rows.length, i;
            for (i = 0; i < len; i++) {
                $("#weeklist").append("<li id = '" + "FOLD" + results.rows.item(i).qSet_ID + "' onclick='Week(" + results.rows.item(i).qSet_ID + ")'><a href='#Checkquestion'><h3>Week " + results.rows.item(i).q_set_name + "</h3><img src='file:///android_asset/images/Christmas-Folder-Blank-icon.png' width='70px' height='70px' class='ui-li-has-thumb' /></a><a data-role='button' data-icon='delete' data-iconpos='notext' onclick='deleteFolder(" + results.rows.item(i).qSet_ID + ")'>Delete</a></li>");
                $("#weeklist").listview('refresh');
            }
        }, null);
    });
};

function showWeek() {
    $("#QCweek").empty();
    db.transaction(function(tx) {
        tx.executeSql('SELECT q_id, q_num, q_content FROM question WHERE qset_id = (select week_idtemp from temp) ORDER BY q_num', [], function(tx, results) {
            var len = results.rows.length, i;
            var msg;
            for (i = 0; i < len; i++) {
                msg = "<li onclick='questID_temp(" + results.rows.item(i).q_ID + ")'><a href='#EditQuestion'><h4>Q" + results.rows.item(i).q_num + " .</h4><p> " + results.rows.item(i).q_content + "</p><img src='file:///android_asset/images/question.jpg' width='70px' height='70px' class='ui-li-has-thumb' /></a><a data-role='button' data-icon='delete' data-iconpos='notext' onclick='deleteQuestion(" + results.rows.item(i).q_ID + ")'>Delete</a></li>";
                $("#QCweek").append(msg);
            }
            $("#QCweek").listview('refresh');
            $("#Checkquestion").trigger('create');
        }, null);
    });
};

function editQuestion() {
    //$('#EditQuestion').live('pageshow', function() {
    var quest_id_t;

    db.transaction(function(tx) {
        var statement = 'SELECT question_IDtemp FROM temp;';
        tx.executeSql(
                statement,
                [], function(tx, results) {
            var len4 = results.rows.length;
            quest_id_t = results.rows.item(len4 - 1).question_IDtemp;
        });
    });

    db.transaction(function(tx) {
        var statement = 'SELECT q_id, q_num, q_content, a, b, c, d, e FROM question WHERE q_id= ' + quest_id_t + '';
        tx.executeSql(
                statement,
                [], function(tx, results) {
            document.getElementById("Eqnum").value = results.rows.item(0).q_num;
            document.getElementById("Equestion").value = results.rows.item(0).q_content;
            document.getElementById("Eoption1").value = results.rows.item(0).a;
            document.getElementById("Eoption2").value = results.rows.item(0).b;
            document.getElementById("Eoption3").value = results.rows.item(0).c;
            document.getElementById("Eoption4").value = results.rows.item(0).d;
            document.getElementById("Eoption5").value = results.rows.item(0).e;
            document.getElementById("EFQid").value = quest_id_t;
            document.getElementById("qesID").value = results.rows.item(0).q_ID;
        });
    });
};


function showQuest() {
    $("#Qexist").empty();
    db.transaction(function(tx) {
        tx.executeSql('SELECT q_num, q_content FROM question WHERE qset_id = (select week_idtemp from temp) ORDER BY q_num', [], function(tx, results) {
            var len = results.rows.length, i;
            var msg;
            for (i = 0; i < len; i++) {
                msg = "<li id='extq'>Q" + results.rows.item(i).q_num + " . " + results.rows.item(i).q_content + "</li>";
                $("#Qexist").append(msg);
            }
            $("#Qexist").listview('refresh');
        }, null);
    });
};

//show graph function
function showGraph()
{
    //lecturerid
    //scode
    //QuestionsetID
    //questionnumberid
    var setIP = document.getElementById("setIP").value;
    var str = document.getElementById("lecturerid").value;
    var str2 = document.getElementById("scode").value;
    str2 = str2.toUpperCase();
    var str3 = document.getElementById("QuestionsetID").value;
    var str4 = document.getElementById("questionnumberid").value;

    if (setIP == "") {
        setIP = "10.0.2.2";
    }

    if (str == "")
    {
        document.getElementById("GraphResult").innerHTML = "";
        return;
    }
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            db.transaction(function(tx) {

                var statement = 'select q_num, q_content from question where qset_id = (select qset_id from questionset where sub_id =(select sub_id from subject where subject_code = "' + str2 + '")) and q_num=' + str4 + '';


                tx.executeSql(statement, [], function(tx, results) {
                    //alert(xmlhttp.responseText);
                    var responsestr = xmlhttp.responseText;
                    var response = responsestr.split(",");

                    //alert(results.rows.item(0).q_num);
                    if (response[0] == 0 && response[1] == 0 && response[3] == 0 && response[4] == 0) {
                        alert("No SMS result for this question/Question is not exist");
                        $('#GraphResult').empty();
                        $('#emailbutton').empty();
                    }
                    else if (results.rows.length == 0) {
                        alert("Question is not exist");
                        $('#GraphResult').empty();
                        $('#emailbutton').empty();
                    }
                    else {
                        $('#emailbutton').empty();
                        var titlegraph = results.rows.item(0).q_content;
                        var qnumgraph = results.rows.item(0).q_num;
                        var data = [
                            ['ResponseA', parseFloat(response[0])], ['ResponseB', parseFloat(response[1])], ['ResponseC', parseFloat(response[2])],
                            ['ResponseD', parseFloat(response[3])], ['ResponseE', parseFloat(response[4])]];
                        var plot1 = jQuery.jqplot('GraphResult', [data],
                                {
                                    seriesDefaults: {
                                        // Make this a pie chart.
                                        renderer: jQuery.jqplot.PieRenderer,
                                        rendererOptions: {
                                            // Put data labels on the pie slices.
                                            // By default, labels show the percentage of the slice.
                                            showDataLabels: true,
                                            dataLabelThreshold: 1,
                                            dataLabels: response,
                                                                                                    dataLabelPositionFactor:0.5,
                                                                                                    sliceMargin:2,
                                            dataLabelFormatString: "%s",
                                                                                                    startAngle: 90
                                        }
                                    },
                                    title: {
                                        text: 'Q' + qnumgraph + ' ' + titlegraph, // title for the plot,
                                        show: true,
                                        fontSize: '10pt',
                                    },
                                    legend: {show: true, fontSize: "10px", location: 'e', marginTop: 10}
                                }
                        );
                        $('#emailbutton').append("<input type='button'  value='Send to Email' onclick='uploadGraph()'/>");
                    }
                });
            });
        }
    }
    var url_serv = "http://" + setIP + "/AndroidServer/retrievegraph.php?lecturerid=" + str + "&scode=" + str2 + "&QuestionsetID=" + str3 + "&questionnumberid=" + str4;
    xmlhttp.open("GET", url_serv, true);
    xmlhttp.send();

};

//get the week id for create question
function Week(idWeek) {
    db.transaction(function(tx) {
        //update week_idtemp
        tx.executeSql('UPDATE temp set week_idtemp = ' + idWeek + ' where sub_idtemp = (select sub_idtemp from temp)');
        showWeek();
    });
};

function back(idWeek) {
    showWeek();
};

//jquery on addQuestion page


function updateQuestion(quest_id_t) {
    //$("#EditQuestion").live('pageshow', function() {
    //$('#Edit_Question').click(function(){
    var Eqnum = document.getElementById("Eqnum").value;
    var Eqq = document.getElementById("Equestion").value;
    var EFQid = document.getElementById("EFQid").value = quest_id_t;
    var Eopt1 = document.getElementById("Eoption1").value;
    var Eopt2 = document.getElementById("Eoption2").value;
    var Eopt3 = document.getElementById("Eoption3").value;
    var Eopt4 = document.getElementById("Eoption4").value;
    var Eopt5 = document.getElementById("Eoption5").value;

    //is used for update table
    db.transaction(function(tx) {
        var stm = ' ' +
                'UPDATE question ' +
                'SET q_num=' + Eqnum + ', q_content="' + Eqq + '", ' +
                '    a="' + Eopt1 + '", b="' + Eopt2 + '", c="' + Eopt3 + '", ' +
                '    d="' + Eopt4 + '", e="' + Eopt5 + '" ' +
                'Where q_id=' + quest_id_t;
        tx.executeSql(stm);
    });

    window.location.hash = "Checkquestion";
};

function uploadGraph() {
    //lecturerid
    //scode
    //QuestionsetID
    //questionnumberid
    var setIP = document.getElementById("setIP").value;
    var str = document.getElementById("lecturerid").value;
    var str2 = document.getElementById("scode").value;
    var email = document.getElementById("mail").value;
    str2 = str2.toUpperCase();
    var str3 = document.getElementById("QuestionsetID").value;
    var str4 = document.getElementById("questionnumberid").value;

    if (setIP == "") {
        setIP = "10.0.2.2";
    }

    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {

        }
    }

    if(email == "" || email == null){
            alert("Please fill your email on settings");
            window.location.hash = 'setting';
    }else{
        db.transaction(function(tx) {
            var statement = 'select q_num, q_content from question where qset_id = (select qset_id from questionset where sub_id =(select sub_id from subject where subject_code = "' + str2 + '")) and q_num=' + str4 + '';

            tx.executeSql(statement, [], function(tx, results) {
                var titlegraph = results.rows.item(0).q_content;
                var qnumgraph = results.rows.item(0).q_num;

                var string11 = jqplotToImg($('#GraphResult'));
                string11 = string11.replace("data:image/png;base64,", "");

                //var graphstring = $('#GraphResult').jqplotToImageStr({});	
                //graphstring = graphstring.replace("data:image/png;base64,", "");

                var folder = "Graph";
                var filename = str + "Week" + str3 + "subject" + str2 + "num" + str4;

                var url_serv = "http://" + setIP + "/AndroidServer/uploadGraphImage.php";

                xmlhttp.open("POST", url_serv, true);
                var formData = new FormData();
                formData.append("image", string11);
                formData.append("folder", folder);
                formData.append("filename", filename);
                xmlhttp.send(formData);
                //alert("Success!");

                //send graph to email
                alert("Your graph has been sent to your email");
                //post method to send to email
                xmlhttpx = new XMLHttpRequest();
                var url_serv2 = "http://" + setIP + "/AndroidServer/sendGraphToEmail.php";
                xmlhttpx.open("POST", url_serv2, true);
                var formDatax = new FormData();
                formDatax.append("address", email);
                formDatax.append("filename", filename);
                xmlhttpx.send(formDatax);
            });
        });
    }
};

//set email for user
function setMail(val) {
    var filter = /\S+@\S+\.\S+/;
    if (filter.test(val)) {
        db.transaction(function(tx) {
            tx.executeSql('INSERT INTO email(email) VALUES ("' + val + '") ');
            alert("email updated");
            window.location.hash = 'mainpage';
        });
    } else {
        alert("email not valid");
    }
};

//get email of user
function sendEmail() {
    uploadGraph();
};


function jqplotToImg(obj) {
    var newCanvas = document.createElement("canvas");
    newCanvas.width = obj.find("canvas.jqplot-base-canvas").width();
    newCanvas.height = obj.find("canvas.jqplot-base-canvas").height() + 10;
    var baseOffset = obj.find("canvas.jqplot-base-canvas").offset();

    // make white background for pasting
    var context = newCanvas.getContext("2d");
    context.fillStyle = "rgba(255,255,255,1)";
    context.fillRect(0, 0, newCanvas.width, newCanvas.height);

    obj.children().each(function() {
        // for the div's with the X and Y axis
        if ($(this)[0].tagName.toLowerCase() == 'div') {
            // X axis is built with canvas
            $(this).children("canvas").each(function() {
                var offset = $(this).offset();
                newCanvas.getContext("2d").drawImage(this,
                        offset.left - baseOffset.left,
                        offset.top - baseOffset.top
                        );
            });
            // Y axis got div inside, so we get the text and draw it on the canvas
            $(this).children("div").each(function() {
                var offset = $(this).offset();
                var context = newCanvas.getContext("2d");
                context.font = $(this).css('font-style') + " " + $(this).css('font-size') + " " + $(this).css('font-family');
                context.fillStyle = $(this).css('color');
                context.fillText($(this).text(),
                        offset.left - baseOffset.left,
                        offset.top - baseOffset.top + $(this).height()
                        );
            });
        } else if ($(this)[0].tagName.toLowerCase() == 'canvas') {
            // all other canvas from the chart
            var offset = $(this).offset();
            newCanvas.getContext("2d").drawImage(this,
                    offset.left - baseOffset.left,
                    offset.top - baseOffset.top
                    );
        }
    });

    // add the point labels
    obj.children(".jqplot-point-label").each(function() {
        var offset = $(this).offset();
        var context = newCanvas.getContext("2d");
        context.font = $(this).css('font-style') + " " + $(this).css('font-size') + " " + $(this).css('font-family');
        context.fillStyle = $(this).css('color');
        context.fillText($(this).text(),
                offset.left - baseOffset.left,
                offset.top - baseOffset.top + $(this).height() * 3 / 4
                );
    });

    // add the title
    obj.children("div.jqplot-title").each(function() {
        var offset = $(this).offset();
        var context = newCanvas.getContext("2d");
        context.font = $(this).css('font-style') + " " + $(this).css('font-size') + " " + $(this).css('font-family');
        context.textAlign = $(this).css('text-align');
        context.fillStyle = $(this).css('color');
        context.fillText($(this).text(),
                newCanvas.width / 2,
                offset.top - baseOffset.top + $(this).height()
                );
    });

    // add the legend
    obj.children("table.jqplot-table-legend").each(function() {
        var offset = $(this).offset();
        var context = newCanvas.getContext("2d");
        context.strokeStyle = $(this).css('border-top-color');
        context.strokeRect(
                offset.left - baseOffset.left,
                offset.top - baseOffset.top,
                $(this).width(), $(this).height()
                );
        context.fillStyle = $(this).css('background-color');
        context.fillRect(
                offset.left - baseOffset.left,
                offset.top - baseOffset.top,
                $(this).width(), $(this).height()
                );
    });

    // add the rectangles
    obj.find("div.jqplot-table-legend-swatch").each(function() {
        var offset = $(this).offset();
        var context = newCanvas.getContext("2d");
        context.fillStyle = $(this).css('background-color');
        context.fillRect(
                offset.left - baseOffset.left,
                offset.top - baseOffset.top,
                $(this).parent().width(), $(this).parent().height()
                );
    });

    obj.find("td.jqplot-table-legend").each(function() {
        var offset = $(this).offset();
        var context = newCanvas.getContext("2d");
        context.font = $(this).css('font-style') + " " + $(this).css('font-size') + " " + $(this).css('font-family');
        context.fillStyle = $(this).css('color');
        context.textAlign = $(this).css('text-align');
        context.textBaseline = $(this).css('vertical-align');
        context.fillText($(this).text(),
                offset.left - baseOffset.left + 25,
                offset.top - baseOffset.top + $(this).height() / 2 + parseInt($(this).css('padding-top').replace('px', ''))
                );
    });

    // convert the image to base64 format
    return newCanvas.toDataURL("image/png");
}

//function to show the answer that stored in database
function showAnswer()
{
    var setIP = document.getElementById("setIP").value;
    var str = document.getElementById("lectureid").value;
    var str2 = document.getElementById("subcode").value;
    var str3 = document.getElementById("questionsetID").value;
    if (setIP == "") {
        setIP = "10.0.2.2";
    }

    if (str == "")
    {
        document.getElementById("AnswerResult").innerHTML = "";
        return;
    }
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            document.getElementById("AnswerResult").innerHTML = xmlhttp.responseText;
        }
    }
    var url_serv = "http://" + setIP + "/AndroidServer/retrieve.php?lectureid=" + str + "&subcode=" + str2 + "&questionsetID=" + str3;
    xmlhttp.open("GET", url_serv, true);
    //alert(url_serv);
    //xmlhttp.open("GET", "http:///AndroidServer/retrieve.php?lectureid=" + str + "&subcode=" + str2 + "&questionsetID=" + str3, true);
    xmlhttp.send();
};

function drawImageHTML()
{
    drawQes();
    uploadQuestionImageHTML();
};

function uploadQuestionImageHTML()
{
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            //not working
            //document.getElementById("statusResult").innerHTML = xmlhttp.responseText;
        }
    }

    db.transaction(function(tx) {
        tx.executeSql('SELECT sub_IDtemp, week_IDtemp FROM temp;', [], function(tx, results) {
            var sID = results.rows.item(0).sub_IDtemp;
            var wID = results.rows.item(0).week_IDtemp
            var statement2 = 'SELECT questionset.q_set_name, questionset.qset_id, questionset.sub_id, subject.subject_code, subject.sub_id FROM questionset, subject WHERE subject.sub_id = questionset.sub_id AND questionset.qset_id = ' + wID;
            tx.executeSql(
                    statement2,
                    [], function(tx, results) {

                var sName = results.rows.item(0).subject_code;
                var wName = results.rows.item(0).q_set_name;
                var qName = document.getElementById('Eqnum').value;
                var email = document.getElementById("mail").value;
                var setIP = document.getElementById("setIP").value;
                if (setIP == "") {
                    setIP = "10.0.2.2";
                }
                                            if(email == "" || email == null){
                                                    alert("Please fill your email on settings");
                                                    window.location.hash = 'setting';
                                            }else{
                var dataURL = canvas.toDataURL("image/png");
                dataURL = dataURL.replace("data:image/png;base64,", ""); //string base64 of png
                var lecturerid = "Question" //will define folder name
                var filename = sName + "Week" + wName + "q" + qName; //define name of image

                //post request to upload canvas image to server
                var url_serv = "http://" + setIP + "/AndroidServer/uploadQesImage.php";
                xmlhttp.open("POST", url_serv, true);
                var formData = new FormData();
                formData.append("image", dataURL);
                formData.append("lecturerid", lecturerid);
                formData.append("filename", filename);
                xmlhttp.send(formData);

                alert("Your picture has been sent to your email");
                //post method to send to email
                xmlhttpx = new XMLHttpRequest();
                var url_serv2 = "http://" + setIP + "/AndroidServer/sendToEmail.php";
                xmlhttpx.open("POST", url_serv2, true);
                var formDatax = new FormData();
                formDatax.append("address", email);
                formDatax.append("filename", filename);
                xmlhttpx.send(formDatax);
                                            }
            }, null);
        }, null);
    });

    /*
     var dataURL= canvas.toDataURL("image/png");
     dataURL = dataURL.replace("data:image/png;base64,", ""); //string base64 of png
     var lecturerid = "Testo" //will define folder name
     var filename = "Vincentbajingan"; //define name of image

     xmlhttp.open("POST", "http://192.168.0.110/AndroidServer/uploadQesImage.php", true);

     var formData = new FormData();
     formData.append("image", dataURL);
     formData.append("lecturerid", lecturerid);
     formData.append("filename", filename);
     xmlhttp.send(formData);
     alert("Success!(seharusnya)");
     */
};

//draw question on canvas
function drawQes() {

    var qi = document.getElementById('qesID').value;
    var qn = document.getElementById('Eqnum').value;
    var qc = document.getElementById('Equestion').value
    var ac = document.getElementById('Eoption1').value;
    var bc = document.getElementById('Eoption2').value;
    var cc = document.getElementById('Eoption3').value;
    var dc = document.getElementById('Eoption4').value;
    var ec = document.getElementById('Eoption5').value;

    var canvas = document.getElementById("canvas");
    ctx = canvas.getContext('2d');
    font_size = 32; // px
    ctx.font = "Bold " + font_size + "px Arial";
    var lines = fragmentText(qn + ". " + qc, canvas.width * 0.9, ctx);

    ctx.save();
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    //background color 
    ctx.fillStyle = '#FFFFFF'; //white
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    //text color
    ctx.fillStyle = '#000000'; //black
    var k = 0;
    lines.forEach(function(line, i) {
        ctx.fillText(line, 0, (i + 1) * font_size);
        k += 1;
    });

    //draw choice per line
    if ($.trim(ac) != "")
    {
        k += 1;
        ctx.fillText('a. ' + document.getElementById('Eoption1').value, 0, (k + 1) * font_size);
    }

    if ($.trim(bc) != "")
    {
        k += 1;
        ctx.fillText('b. ' + document.getElementById('Eoption2').value, 0, (k + 1) * font_size);
    }

    if ($.trim(cc) != "")
    {
        k += 1;
        ctx.fillText('c. ' + document.getElementById('Eoption3').value, 0, (k + 1) * font_size);
    }

    if ($.trim(dc) != "")
    {
        k += 1;
        ctx.fillText('d. ' + document.getElementById('Eoption4').value, 0, (k + 1) * font_size);
    }

    if ($.trim(ec) != "")
    {
        k += 1;
        ctx.fillText('e. ' + document.getElementById('Eoption5').value, 0, (k + 1) * font_size);
    }

    ctx.restore();

    //is used for update table
    db.transaction(function(tx) {
        var stm = ' ' +
                'UPDATE question ' +
                'SET q_num=' + qn + ', q_content="' + qc + '", ' +
                '    a="' + ac + '", b="' + bc + '", c="' + cc + '", ' +
                '    d="' + dc + '", e="' + ec + '" ' +
                'Where q_id=' + qi;
        tx.executeSql(stm);
    });
}

//function used in drawQes()
function fragmentText(text, maxWidth, ctx) {
    var words = text.split(' '),
            lines = [],
            line = "";

    if (ctx.measureText(text).width < maxWidth) {
        return [text];
    }

    while (words.length > 0) {
        while (ctx.measureText(words[0]).width >= maxWidth) {
            var tmp = words[0];
            words[0] = tmp.slice(0, -1);
            if (words.length > 1) {
                words[1] = tmp.slice(-1) + words[1];
            } else {
                words.push(tmp.slice(-1));
            }
        }
        if (ctx.measureText(line + words[0]).width < maxWidth) {
            line += words.shift() + " ";
        } else {
            lines.push(line);
            line = "";
        }
        if (words.length === 0) {
            lines.push(line);
        }
    }
    return lines;
};

function deleteSubject(idSubject) {
    var y = confirm("are you sure want to delete this subject?");
    if (y == true) {
        //statement to delete subject
        db.transaction(function(tx) {
            tx.executeSql('DELETE FROM subject WHERE sub_ID = "' + idSubject + '";');
        });

        //delete list in delete list subject
        var id1 = "#delSUB" + idSubject;
        $(id1).remove();

        //delete list in subject list
        var id1 = "#SUB" + idSubject;
        $(id1).remove();

        //back to home
        window.location.hash = 'home';
    }
}

//function for temp the sub_ID
function subID_temp(idSubject) {
    db.transaction(function(tx) {
        tx.executeSql('DELETE FROM temp;');
        tx.executeSql('INSERT INTO temp(sub_idtemp) VALUES(' + idSubject + ');');
    });
}

//function for temp the question
function questID_temp(idQuest) {
    db.transaction(function(tx) {
        tx.executeSql('update temp set question_idtemp = ' + idQuest + ' \
                       where sub_idtemp= (SELECT sub_IDtemp FROM temp)');
    });
}

function del() {
    $("#QCweek").empty();
}

function deleteFolder(idSubject) {
    var y = confirm("are you sure want to delete this folder?");
    if (y == true) {
        //statement to delete subject
        db.transaction(function(tx) {
            tx.executeSql('DELETE FROM questionSet WHERE qSet_ID = "' + idSubject + '";');
        });

        //delete list in delete list subject
        var id3 = "#delFOLD" + idSubject;
        $(id3).remove();

        //delete list in subject list
        var id4 = "#FOLD" + idSubject;
        $(id4).remove();

        //back to home
        window.location.hash = 'your_folder';
    }
}

function deleteQuestion(idSubject) {
    var y = confirm("are you sure want to delete this question?");
    if (y == true) {
        //statement to delete question
        db.transaction(function(tx) {
            tx.executeSql('DELETE FROM question WHERE q_ID = "' + idSubject + '";');
        });

        //delete list in delete list subject
        var id5 = "#delQuestion" + idSubject;
        $(id5).remove();

        //back to folder
        window.location.hash = 'your_folder';
    }
}

function drawImageAndroid()
{
    var id = document.getElementById("qesID").value;
    db.transaction(function(tx) {
        var statement = 'SELECT q_id, q_num, q_content, a, b, c, d, e FROM question WHERE q_id= ' + id + '';
        tx.executeSql(
                statement,
                [], function(tx, results) {
            var num = results.rows.item(0).q_num;
            var content = results.rows.item(0).q_content;
            var as = results.rows.item(0).a;
            var bs = results.rows.item(0).b;
            var cs = results.rows.item(0).c;
            var ds = results.rows.item(0).d;
            var es = results.rows.item(0).e;
            Android.createImage(id, num, content, as, bs, cs, ds, es);
        });
    });
}


