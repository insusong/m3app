/* 
 * The MIT License
 *
 * Copyright 2014 insu song.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var m3Main = {
    initialize: function () {

        //CREATE TABLE DB
        db.transaction(function(tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS subject (sub_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, subject_code VARCHAR NOT NULL, subject_description VARCHAR NOT NULL);');
            tx.executeSql('CREATE TABLE IF NOT EXISTS questionSet (qSet_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, q_set_name VARCHAR NOT NULL, q_set_description VARCHAR, sub_ID INTEGER, FOREIGN KEY (sub_ID) REFERENCES subject(sub_ID));');
            tx.executeSql('CREATE TABLE IF NOT EXISTS question (q_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, q_num INTEGER NOT NULL, q_content VARCHAR, a VARCHAR, b VARCHAR, c VARCHAR, d VARCHAR, e VARCHAR, qSet_ID INTEGER, FOREIGN KEY (qSet_ID) REFERENCES questionSet(qSet_ID));');
            tx.executeSql('CREATE TABLE IF NOT EXISTS temp (sub_IDtemp INTEGER, question_IDtemp INTEGER, week_IDtemp INTEGER);');
            tx.executeSql('CREATE TABLE IF NOT EXISTS email (email VARCHAR);');
        });

        // when home is pageshow, get subject-list data
        $("#home").live('pageshow', function() {
            $("#subjectlist").empty();
            db.transaction(function(tx) {
                tx.executeSql('SELECT * FROM subject', [], function(tx, results) {
                    var len = results.rows.length, i;

                    for (i = 0; i < len; i++) {
                        $("#subjectlist").append("<li id = '" + "SUB" + results.rows.item(i).sub_ID + "' onclick='subID_temp(" + results.rows.item(i).sub_ID + ")'><a href='#your_folder'><h3>" + results.rows.item(i).subject_code + "</h3><p> " + results.rows.item(i).subject_description + "</p><img src='file:///android_asset/images/Christmas-Folder-Blank-icon.png' width='70px' height='70px' class='ui-li-has-thumb' /></a><a data-role='button' data-icon='delete' data-iconpos='notext' onclick='deleteSubject(" + results.rows.item(i).sub_ID + ")'>Delete</a> </li>");
                        $("#subjectlist").listview('refresh');
                    }
                }, null);
            });
        });

        //show the new page after refresh in your folder
        $('#your_folder').live('pageshow', function() {
            showlistview();
        });
        
        $('#mainpage').live('pageshow', function() {
            db.transaction(function(tx) {
                var statement = 'SELECT email FROM email';
                tx.executeSql(statement, [], function(tx, results) {
                    if (results.rows.length == 0) {
                    }
                    else {
                        document.getElementById("mail").value = results.rows.item(results.rows.length - 1).email;
                    }
                }, null);
            });
        });
        
        $('#setting').live('pageshow', function() {
            db.transaction(function(tx) {
                var statement = 'SELECT email FROM email';
                tx.executeSql(statement, [], function(tx, results) {
                    if (results.rows.length == 0) {
                    }
                    else {
                        document.getElementById("mail").value = results.rows.item(results.rows.length - 1).email;
                    }
                }, null);
            });
        });

        $('#Checkquestion').live('pageshow', function() {
            showWeek();
        });

        $('#EditQuestion').live('pageshow', function() {
            editQuestion();
            $('#Edit_Question').click(function() {
                db.transaction(function(tx) {
                    var statement = 'SELECT question_IDtemp FROM temp;';
                    tx.executeSql(
                            statement,
                            [], function(tx, results) {
                        updateQuestion(results.rows.item(results.rows.length - 1).question_IDtemp);
                    });
                });
            });
        });

        //jquery on addQuestion page
        $('#AddQuestion').live('pagecreate', function() {
            showQuest();
            $('#Add_Question').click(function() {
                $('#QCweek').empty();
                var qnum = document.getElementById("qnum").value;
                var qq = document.getElementById("question").value;
                var opt1 = document.getElementById("option1").value;
                var opt2 = document.getElementById("option2").value;
                var opt3 = document.getElementById("option3").value;
                var opt4 = document.getElementById("option4").value;
                var opt5 = document.getElementById("option5").value;

                if (qnum.length == 0 || qq.length == 0) {
                    alert("question number / question cannot be blank");
                    return false;
                }
                else {
                    //insert question to database
                    db.transaction(function(tx) {
                        tx.executeSql('INSERT INTO question (q_num, q_content, a, b, c, d, e, qSet_ID) VALUES ("' + qnum + '", "' + qq + '","' + opt1 + '","' + opt2 + '","' + opt3 + '","' + opt4 + '","' + opt5 + '",(select week_idtemp from temp));');
                    });
                    alert("question has been added");

                    //refresh the question
                    $("#QCweek").empty();
                    db.transaction(function(tx) {
                        tx.executeSql('SELECT q_num, q_content FROM question where qset_id=(select week_idtemp from temp)', [], function(tx, results) {
                            var len = results.rows.length, i;
                            msg = "<li id='newq'>Q" + results.rows.item(len - 1).q_num + " . " + results.rows.item(len - 1).q_content + "</li>";
                            $("#NQexist").append(msg);
                            $("#NQexist").listview('refresh');
                            $("#AddQuestion").trigger('create');

                        }, null);
                    });

                    showQuest();

                    //empty the value of the textbox on addquestion
                    document.getElementById('qnum').value = "";
                    document.getElementById('question').value = "";
                    document.getElementById('option1').value = "";
                    document.getElementById('option2').value = "";
                    document.getElementById('option3').value = "";
                    document.getElementById('option4').value = "";
                    document.getElementById('option5').value = "";
                }
            });
        });

        //ADD SUBJECT JQUERY
        $('#add_subject').live('pagecreate', function() {
            $('#addSubjectButton').click(function() {
                //retrieve value from input box			
                var code = document.getElementById('subject_id').value;
                code = code.toUpperCase();
                var desc = document.getElementById('subject_name').value;

                //insert to database
                db.transaction(function(tx) {
                    tx.executeSql('INSERT INTO subject (subject_code, subject_description) VALUES ("' + code + '", "' + desc + '");');
                });

                //retrieve the sub_ID
                db.transaction(function(tx) {
                    var statement = 'SELECT * FROM subject;';
                    tx.executeSql(statement, [], function(tx, results) {
                        var len = results.rows.length, i;
                        for (i = 0; i < len; i++) {
                            //refresh the list
                            $("#subjectlist").append("<li id = '" + "SUB" + results.rows.item(i).sub_ID + "' onclick='subID_temp(" + results.rows.item(i).sub_ID + ")'><a href='#your_folder'>" + code + " " + desc + "</a></li>");
                            $("#subjectlist").listview('refresh');
                            $('#home').trigger('create');
                        }
                    }, null);
                });

                document.getElementById('subject_id').value = "";
                document.getElementById('subject_name').value = "";

                //back to home
                window.location.hash = 'home';
            });
        });

        //CREATE FOLDER JQUERY
        $('#create_folder').live('pagecreate', function() {
            $('#CreateFolderButton').click(function() {
                //retrieve value from input box
                var week_input = document.getElementById("week").value;
                //var desc_input = document.getElementById("desc").value;
                var sub_id2;

                //open DB temp that store sub_ID and try to get the value
                db.transaction(function(tx) {
                    var statement = 'SELECT sub_IDtemp FROM temp;';
                    tx.executeSql(statement, [], function(tx, results) {
                        var len = results.rows.length;
                        sub_id2 = results.rows.item(len - 1).sub_IDtemp;
                    });
                });

                //code for insert to database
                db.transaction(function(tx) {
                    tx.executeSql('INSERT INTO questionSet(q_set_name, sub_ID) VALUES ("' + week_input + '",' + sub_id2 + ');');
                });

                //emptify the input box
                document.getElementById('week').value = "";

                //back to your folder
                window.location.hash = 'your_folder';
            });
        });        

    //---        
    }
};

